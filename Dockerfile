FROM python:3 AS development

RUN pip install pipenv

ENV PIPENV_VENV_IN_PROJECT true
ENV FLASK_APP main

FROM development

WORKDIR /app
COPY / /app

ENV PATH $PATH:/app/.venv/bin
ENV PYTHONPATH /app/src/

RUN pipenv install

EXPOSE 80

WORKDIR /app/src/
ENTRYPOINT [ "flask" ]
CMD [ "run", "--host=0.0.0.0", "--port=80" ]
