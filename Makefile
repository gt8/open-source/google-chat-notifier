.PHONY: test

up:
	@flask run --host=0.0.0.0 --port=80

deps:
	@pipenv install -d

test:
	@pytest

dshell:
	@docker-compose run --rm --service-ports --entrypoint=bash python

dbuild:
	@docker-compose build

dclean:
	@docker-compose down -v --rmi=local
