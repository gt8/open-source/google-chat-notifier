"""
GitLab Notifier provides GitLab Webhook Payload Parsing and Notifications
"""
from typing import Optional
import importlib

# We'll make these configurable through the environment :smile:
SUPPORTED_EVENTS = [
    'issue',
    'merge_request',
    'note'
]

SUPPORTED_ACTIONS = [
    'user_assignments',
    'wip_merge_request',
    'user_comment'
]


def discover(payload: dict) -> tuple:
    event = discover_event(payload)
    actions = discover_actions(payload)

    return (event, actions)


def discover_event(payload: dict) -> Optional[dict]:
    """
    Loop through all SUPPORTED_EVENTS until we find a match and return
    """
    for supported_event in SUPPORTED_EVENTS:
        module = importlib.import_module('gitlab.events.' + supported_event)
        event = module.discover(payload)

        if event is not None:
            return event

    return None


def discover_actions(payload: dict) -> list:
    """
    Loop through all SUPPORTED_ACTIONS and build up a list of them from the
    payload
    """
    actions = []

    for supported_actions in SUPPORTED_ACTIONS:
        module = importlib.import_module('gitlab.actions.' + supported_actions)
        actions += module.discover(payload)

    return actions
