from google_chat.widgets import key_value


class UserAssigned:
    """
    UserAssigned Event
    """

    def __init__(self, name, username, avatar):
        self.name = name
        self.username = username
        self.avatar = avatar

    def render(self):
        return [
            key_value(
                top='User Assigned',
                content=self.name,
                image=self.avatar
            )
        ]

    def __eq__(self, other):
        return self.username == other.username

    def __ne__(self, other):
        return self.username != other.username


class UserUnassigned:
    """
    UserUnassigned Event
    """

    def __init__(self, name, username, avatar):
        self.namespace = "user_assignments"
        self.template = "unassigned"
        self.name = name
        self.username = username
        self.avatar = avatar

    def render(self):
        return [
            key_value(
                top='User Unassigned',
                content=self.name,
                image=self.avatar
            )
        ]

    def __eq__(self, other):
        return self.username == other.username

    def __ne__(self, other):
        return self.username != other.username


def discover(payload: str) -> list:
    """
    Discover UserAssigned and UserUnassigned events from a GitLab payload
    """
    if 'changes' not in payload:
        return []

    if 'assignees' not in payload['changes']:
        return []

    previous_assignees = {}
    assignee_events = []

    if 'previous' in payload['changes']['assignees']:
        for previous_assignee in payload['changes']['assignees']['previous']:
            previous_assignees[previous_assignee['username']
                               ] = previous_assignee

    if 'current' in payload['changes']['assignees']:
        for current_assignee in payload['changes']['assignees']['current']:
            if current_assignee['username'] in previous_assignees:
                previous_assignees.pop(current_assignee['username'])
            else:
                assignee_events.append(UserAssigned(
                    current_assignee['name'], current_assignee['username'], current_assignee['avatar_url']))

    for _, assignee in previous_assignees.items():
        assignee_events.append(UserUnassigned(
            assignee['name'], assignee['username'], assignee['avatar_url']))

    return assignee_events
