import unittest

import gitlab.actions.user_assignments as uut


class GitLabUserAssignedEventsTestCase(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_it_returns_empty_list_if_nothing_to_discover(self):
        payload = {}
        self.assertEqual(uut.discover(payload), [])

        payload = {'changes': {}}
        self.assertEqual(uut.discover(payload), [])

        payload = {'changes': {'assignees': {}}}
        self.assertEqual(uut.discover(payload), [])

    def test_it_handles_assignments(self):
        event = {'changes': {
            'assignees': {
                'previous': [],
                'current': [
                    {
                        'username': 'username',
                        'name': 'Name',
                        'avatar_url': 'avatar'
                    }
                ]
            }
        }}

        discovered_events = uut.discover(event)

        self.assertEqual(
            [uut.UserAssigned('Name', 'username', 'avatar')], discovered_events)

    def test_it_handles_unassignments(self):
        event = {'changes': {
            'assignees': {
                'previous': [
                    {
                        'username': 'username',
                        'name': 'Name',
                        'avatar_url': 'avatar'
                    }
                ],
                'current': []
            }
        }}

        discovered_events = uut.discover(event)

        self.assertEqual(
            [uut.UserUnassigned('Name', 'username', 'avatar')], discovered_events)

    def test_it_handles_both_assignments_and_unassignments(self):
        event = {'changes': {
            'assignees': {
                'previous': [
                    {
                        'username': 'username',
                        'name': 'Name',
                        'avatar_url': 'avatar'
                    }
                ],
                'current': [
                    {
                        'username': 'username2',
                        'name': 'Name',
                        'avatar_url': 'avatar'
                    }
                ]
            }
        }}

        discovered_events = uut.discover(event)

        self.assertEqual(
            [uut.UserAssigned('Name', 'username2', 'avatar'), uut.UserUnassigned('Name', 'username', 'avatar')], discovered_events)

    def test_it_handles_multiple_assignments_and_unassignments(self):
        event = {'changes': {
            'assignees': {
                'previous': [
                    {
                        'username': 'username1',
                        'name': 'Name',
                        'avatar_url': 'avatar'
                    },
                    {
                        'username': 'username2',
                        'name': 'Name',
                        'avatar_url': 'avatar'
                    },
                    {
                        'username': 'username3',
                        'name': 'Name',
                        'avatar_url': 'avatar'
                    }
                ],
                'current': [
                    {
                        'username': 'username2',
                        'name': 'Name',
                        'avatar_url': 'avatar'
                    },
                    {
                        'username': 'username4',
                        'name': 'Name',
                        'avatar_url': 'avatar'
                    },
                    {
                        'username': 'username5',
                        'name': 'Name',
                        'avatar_url': 'avatar'
                    }
                ]
            }
        }}

        discovered_events = uut.discover(event)

        self.assertEqual([
            uut.UserAssigned('Name', 'username4', 'avatar'),
            uut.UserAssigned('Name', 'username5', 'avatar'),
            uut.UserUnassigned('Name', 'username1', 'avatar'),
            uut.UserUnassigned('Name', 'username3', 'avatar')
        ], discovered_events)


if __name__ == '__main__':
    unittest.main()
