from google_chat.widgets import key_value


class UserComment:
    """
    UserComment Action
    """

    def __init__(self, cid, name, avatar, comment):
        self.comment_id = cid
        self.name = name
        self.avatar = avatar
        self.comment = comment

    def render(self):
        return [
            key_value(
                top=self.name,
                content=self.comment,
                image=self.avatar
            )
        ]

    def __eq__(self, other):
        return self.comment_id == other.comment_id

    def __ne__(self, other):
        return self.comment_id != other.comment_id


def discover(payload: str) -> list:
    """
    Discover Comments
    """
    if 'object_kind' not in payload:
        return []

    if 'note' != payload['object_kind']:
        return []

    if 'object_attributes' not in payload:
        return []

    attributes = payload['object_attributes']

    user = payload['user']

    return [
        UserComment(
            cid=attributes['id'],
            name=user['name'],
            avatar=user['avatar_url'],
            comment=attributes['note']
        )
    ]
