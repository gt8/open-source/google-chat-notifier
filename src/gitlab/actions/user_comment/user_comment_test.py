import gitlab.events.note.mocks as mocks
import gitlab.actions.user_comment as user_comment


def test_discover():
    no_payload = {}
    assert user_comment.discover(no_payload) == []

    assert user_comment.discover(mocks.INVALID_PAYLOAD) == []

    assert user_comment.discover(mocks.NOTE_ISSUE) == [user_comment.UserComment(
        cid=1,
        name='',
        avatar='',
        comment=''
    )]

    assert user_comment.discover(mocks.NOTE_MERGE_REQUEST) == [user_comment.UserComment(
        cid=1,
        name='',
        avatar='',
        comment=''
    )]
