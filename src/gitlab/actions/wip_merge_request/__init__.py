from google_chat.widgets import key_value, text_button


class MergeRequestReadyForReview:
    """
    This action is emitted whenever a merge request is marked as no longer being
    WIP
    """

    def __init__(self, title, url):
        self.title = title
        self.url = url

    def render(self):
        return [
            key_value(bottom='WIP Removed', content='Ready for 👀')
        ]

    def __eq__(self, other):
        return self.url == other.url

    def __ne__(self, other):
        return self.url != other.url


def discover(payload: str) -> list:
    """
    Discover when a MergeRequest has been unmarked WIP
    """
    if 'changes' not in payload:
        return []

    if 'object_attributes' not in payload:
        return[]

    if 'action' not in payload['object_attributes']:
        return[]

    if 'update' not in payload['object_attributes']['action']:
        return []

    if 'title' not in payload['changes']:
        return []

    previous_title = payload['changes']['title']['previous']
    current_title = payload['changes']['title']['current']

    if False is previous_title.startswith("WIP:"):
        return []
    if True is current_title.startswith("WIP:"):
        return []

    title = payload['object_attributes']['title']
    url = payload['object_attributes']['url']

    return [MergeRequestReadyForReview(title=title, url=url)]
