INVALID_PAYLOAD = {'object_kind': 'merge_request'}

REMOVING_WIP = {
    'object_kind': 'merge_request',
    'object_attributes': {
        'action': 'update',
        'title': 'Title',
        'url': 'url'
    },
    'changes': {
        'title': {
            'current': 'Title',
            'previous': 'WIP: Title',
        }
    }
}

ADDING_WIP = {
    'object_kind': 'merge_request',
    'object_attributres': {
        'action': 'update',
        'title': 'Title',
        'url': 'url'
    },
    'changes': {
        'title': {
            'current': 'WIP: Title',
            'previous': 'Title',
        }
    }
}
