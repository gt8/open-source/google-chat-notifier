import gitlab.actions.wip_merge_request as wip_merge_request
import gitlab.actions.wip_merge_request.mocks as mocks


def test_it_can_handle_invalid_events():
    assert [] == wip_merge_request.discover({})
    assert [] == wip_merge_request.discover(mocks.INVALID_PAYLOAD)


def test_it_can_handle_wip_being_removed():
    assert wip_merge_request.discover(
        mocks.REMOVING_WIP) == [wip_merge_request.MergeRequestReadyForReview(
            title='title',
            url='url'
        )]
