from datetime import datetime
from flask import Blueprint, request
from httplib2 import Http
from os import environ
from gitlab import discover
from google_chat.widgets import header, footer

import json
import pprint
import sys

gitlab_blueprint = Blueprint('gitlab', __name__)


@gitlab_blueprint.route("/gitlab", methods=["POST"])
def endpoint():
    payload = request.get_json()

    # Until we understand all the payloads, lets dump them to the console
    # to investigate and add support.
    print("GitLab Payload")
    pprint.pprint(payload)
    sys.stdout.flush()

    (event, actions) = discover(payload)

    if event is None:
        print("No known event. Stopping")
        sys.stdout.flush()
        return '{"message": "Thanks!"}', 200

    if len(actions) == 0 and event.supress_no_actions is True:
        print("No Actions. Stopping")
        sys.stdout.flush()
        return '{"message": "Thanks!"}', 200

    widgets = []
    for action in actions:
        widgets += action.render()

    print("Event URL: " + event.url)
    if event.url is not None:
        widgets.append(footer(event))

    rendered_card = {
        'cards': [
            {
                'header': header(event)
            },
            {
                'sections': [
                    {
                        'widgets': widgets
                    }
                ]
            }
        ]
    }

    json_message = json.dumps(rendered_card)
    pprint.pprint(json_message)
    sys.stdout.flush()

    now = datetime.now()
    thread_key = now.strftime("%Y-%m-%d")

    notification_request = Http()

    (response, content) = notification_request.request(
        uri=environ["WEBHOOK_URI"] + "&threadKey=" + thread_key,
        method='POST',
        headers={'Content-Type': 'application/json; charset=UTF-8'},
        body=json_message,
    )

    pprint.pprint(response)
    pprint.pprint(content)
    sys.stdout.flush()

    return '{"message": "Thanks!"}', 200
