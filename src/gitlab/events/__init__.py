class Event:
    """
    GitLab Event
    """

    def __init__(self, id, type, title, subtitle=None, content=None, image=None, image_style='IMAGE', url=None):
        self.id = id
        self.type = type
        self.supress_no_actions = False
        self.title = title
        self.subtitle = subtitle
        self.content = content
        self.image = image
        self.image_style = image_style
        self.url = url

    def __eq__(self, other):
        if self.id != other.id:
            return False
        return True

    def __ne__(self, other):
        if self.id != other.id:
            return True
        return False
