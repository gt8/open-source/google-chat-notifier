from typing import Optional
from gitlab.events import Event


class IssueOpened(Event):
    def __init__(self, id, title, url):
        Event.__init__(
            self,
            id=id,
            type='issue.opened',
            subtitle='Issue Opened',
            title=title,
            image='https://raw.githubusercontent.com/webdog/octicons-png/master/black/issue-opened.png',
            url=url
        )
        self.supress_no_actions = False


class IssueClosed(Event):
    def __init__(self, id, title, url):
        Event.__init__(
            self,
            id=id,
            type='issue.closed',
            subtitle='Issue Closed',
            title=title,
            image='https://raw.githubusercontent.com/webdog/octicons-png/master/black/issue-closed.png',
            url=url
        )
        self.supress_no_actions = False


class IssueUpdated(Event):
    def __init__(self, id, title, url):
        Event.__init__(
            self,
            id=id,
            type='issue.updated',
            subtitle='Issue Updated',
            title=title,
            image='https://raw.githubusercontent.com/webdog/octicons-png/master/black/issue-opened.png',
            url=url
        )
        self.supress_no_actions = True


def discover(payload: dict) -> Optional[dict]:
    if 'object_kind' not in payload:
        return None
    if payload['object_kind'] != 'issue':
        return None
    if 'object_attributes' not in payload:
        return None

    attributes = payload['object_attributes']

    if 'action' not in attributes:
        return None

    action = attributes['action']

    if action == 'open':
        return IssueOpened(id=attributes['id'], title=attributes['title'], url=attributes['url'])
    elif action == 'close':
        return IssueClosed(id=attributes['id'], title=attributes['title'], url=attributes['url'])
    elif action == 'update':
        return IssueUpdated(id=attributes['id'], title=attributes['title'], url=attributes['url'])

    return None
