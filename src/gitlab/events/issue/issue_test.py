import gitlab.events.issue as issue
import gitlab.events.issue.mocks as mocks


def test_discover():
    no_payload = {}
    assert issue.discover(no_payload) is None

    assert issue.discover(mocks.INVALID_PAYLOAD) is None

    assert issue.discover(mocks.ISSUE_OPENED) == issue.IssueOpened(
        id=1,
        url='url',
        title='My Awesome Title'
    )

    assert issue.discover(mocks.ISSUE_CLOSED) == issue.IssueClosed(
        id=1,
        url='url',
        title='My Awesome Title'
    )

    assert issue.discover(mocks.ISSUE_UPDATED) == issue.IssueUpdated(
        id=1,
        url='url',
        title='My Awesome Title'
    )
