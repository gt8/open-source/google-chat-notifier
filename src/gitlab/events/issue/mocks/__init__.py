INVALID_PAYLOAD = {
    'object_kind': 'issue'
}

ISSUE_OPENED = {
    'object_kind': 'issue',
    'object_attributes': {
        'id': 1,
        'action': 'open',
        'title': 'My Awesome Title',
        'url': 'url'
    }
}

ISSUE_CLOSED = {
    'object_kind': 'issue',
    'object_attributes': {
        'id': 1,
        'action': 'close',
        'title': 'My Awesome Title',
        'url': 'url'
    }
}

ISSUE_UPDATED = {
    'object_kind': 'issue',
    'object_attributes': {
        'id': 1,
        'action': 'update',
        'title': 'My Awesome Title',
        'url': 'url'
    }
}
