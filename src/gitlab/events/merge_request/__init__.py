from typing import Optional
from gitlab.events import Event


class MergeRequestOpened(Event):
    def __init__(self, id, title, url):
        Event.__init__(
            self,
            id=id,
            type='merge_request.opened',
            subtitle='Merge Request Opened',
            title=title,
            image='https://raw.githubusercontent.com/webdog/octicons-png/master/black/git-pull-request.png',
            url=url
        )
        self.supress_no_actions = False


class MergeRequestClosed(Event):
    def __init__(self, id, title, url):
        Event.__init__(
            self,
            id=id,
            type='merge_request.closed',
            subtitle='Merge Request Closed',
            title=title,
            image='https://raw.githubusercontent.com/webdog/octicons-png/master/black/git-pull-request.png',
            url=url
        )
        self.supress_no_actions = False


class MergeRequestUpdated(Event):
    def __init__(self, id, title, url):
        Event.__init__(
            self,
            id=id,
            type='merge_request.updated',
            subtitle='Merge Request Updated',
            title=title,
            image='https://raw.githubusercontent.com/webdog/octicons-png/master/black/git-pull-request.png',
            url=url
        )
        self.supress_no_actions = True


def discover(payload: dict) -> Optional[dict]:
    if 'object_kind' not in payload:
        return None
    if payload['object_kind'] != 'merge_request':
        return None
    if 'object_attributes' not in payload:
        return None

    attributes = payload['object_attributes']

    if 'action' not in attributes:
        return None

    action = attributes['action']

    if action == 'open':
        return MergeRequestOpened(id=attributes['id'], title=attributes['title'], url=attributes['url'])
    elif action == 'close':
        return MergeRequestClosed(id=attributes['id'], title=attributes['title'], url=attributes['url'])
    elif action == 'update':
        return MergeRequestUpdated(id=attributes['id'], title=attributes['title'], url=attributes['url'])

    return None
