import gitlab.events.merge_request as merge_request
import gitlab.events.merge_request.mocks as mocks


def test_discover():
    no_payload = {}
    assert merge_request.discover(no_payload) is None

    assert merge_request.discover(mocks.INVALID_PAYLOAD) is None

    assert merge_request.discover(mocks.MERGE_REQUEST_OPENED) == merge_request.MergeRequestOpened(
        id=1,
        url='url',
        title='My Awesome Title'
    )

    assert merge_request.discover(mocks.MERGE_REQUEST_CLOSED) == merge_request.MergeRequestClosed(
        id=1,
        url='url',
        title='My Awesome Title'
    )

    assert merge_request.discover(mocks.MERGE_REQUEST_UPDATED) == merge_request.MergeRequestUpdated(
        id=1,
        url='url',
        title='My Awesome Title'
    )
