INVALID_PAYLOAD = {
    'object_kind': 'merge_request'
}

MERGE_REQUEST_OPENED = {
    'object_kind': 'merge_request',
    'object_attributes': {
        'id': 1,
        'url': 'url',
        'action': 'open',
        'title': 'My Awesome Title'
    }
}

MERGE_REQUEST_CLOSED = {
    'object_kind': 'merge_request',
    'object_attributes': {
        'id': 1,
        'url': 'url',
        'action': 'close',
        'title': 'My Awesome Title'
    }
}

MERGE_REQUEST_UPDATED = {
    'object_kind': 'merge_request',
    'object_attributes': {
        'id': 1,
        'url': 'url',
        'action': 'update',
        'title': 'My Awesome Title'
    }
}

MERGE_REQUEST_WIP_ADDED = {
    'object_kind': 'merge_request',
    'object_attributes': {
        'id': 1,
        'url': 'url',
        'action': 'update',
        'title': 'My Awesome Title'
    },
    'changes': {
        'title': {
            'current': 'WIP: My Awesome Title',
            'previous': 'My Awesome Title'
        }
    }
}


MERGE_REQUEST_WIP_REMOVED = {
    'object_kind': 'merge_request',
    'object_attributes': {
        'id': 1,
        'url': 'url',
        'action': 'update',
        'title': 'My Awesome Title'
    },
    'changes': {
        'title': {
            'current': 'My Awesome Title',
            'previous': 'WIP: My Awesome Title'
        }
    }
}
