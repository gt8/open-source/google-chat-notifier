from typing import Optional
from gitlab.events import Event


class NoteOnIssue(Event):
    def __init__(self, id, title, url):
        Event.__init__(
            self,
            id=id,
            type='note.issue',
            subtitle='Issue Comment',
            title=title,
            image='https://raw.githubusercontent.com/webdog/octicons-png/master/black/comment.png',
            url=url
        )
        self.supress_no_actions = False


class NoteOnMergeRequest(Event):
    def __init__(self, id, title, url):
        Event.__init__(
            self,
            id=id,
            type='note.merge_request',
            subtitle='Merge Request Comment',
            title=title,
            image='https://raw.githubusercontent.com/webdog/octicons-png/master/black/comment.png',
            url=url
        )
        self.supress_no_actions = False


def discover(payload: dict) -> Optional[dict]:
    if 'object_kind' not in payload:
        return None
    if payload['object_kind'] != 'note':
        return None
    if 'object_attributes' not in payload:
        return None

    attributes = payload['object_attributes']

    if 'noteable_type' not in attributes:
        return None

    noteable_type = attributes['noteable_type']

    if noteable_type == 'Issue':
        if 'issue' not in payload:
            return None
        issue = payload['issue']

        return NoteOnIssue(id=attributes['id'], title=issue['title'], url=issue['url'])
    elif noteable_type == 'MergeRequest':
        if 'merge_request' not in payload:
            return None
        merge_request = payload['merge_request']

        return NoteOnMergeRequest(id=attributes['id'], title=merge_request['title'], url=merge_request['url'])

    return None
