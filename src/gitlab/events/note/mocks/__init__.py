INVALID_PAYLOAD = {
    'object_kind': 'note'
}

NOTE_ISSUE = {
    'object_kind': 'note',
    'object_attributes': {
        'id': 1,
        'noteable_type': 'Issue',
        'note': 'My Comment'
    },
    'issue': {
        'title': 'Issue Title',
        'url': 'url'
    },
    'user': {
        'name': 'My Name',
        'username': 'username',
        'avatar_url': 'url'
    }
}

NOTE_MERGE_REQUEST = {
    'object_kind': 'note',
    'object_attributes': {
        'id': 1,
        'noteable_type': 'MergeRequest',
        'note': 'My Comment'
    },
    'merge_request': {
        'title': 'Merge Request Title',
        'url': 'url'
    },
    'user': {
        'name': 'My Name',
        'username': 'username',
        'avatar_url': 'url'
    }
}
