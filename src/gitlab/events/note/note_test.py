import gitlab.events.note as note
import gitlab.events.note.mocks as mocks


def test_discover():
    no_payload = {}
    assert note.discover(no_payload) is None

    assert note.discover(mocks.INVALID_PAYLOAD) is None

    assert note.discover(mocks.NOTE_ISSUE) == note.NoteOnIssue(
        id=1,
        url='url',
        title='Issue Title'
    )

    assert note.discover(mocks.NOTE_MERGE_REQUEST) == note.NoteOnMergeRequest(
        id=1,
        url='url',
        title='Issue Title'
    )
