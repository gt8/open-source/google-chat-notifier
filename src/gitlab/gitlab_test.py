from jinja2 import Environment, FileSystemLoader
from os import path
import json

import gitlab
import gitlab.events.issue as issue
import gitlab.events.issue.mocks as issue_mocks
import gitlab.events.merge_request as merge_request
import gitlab.events.merge_request.mocks as merge_request_mocks


def test_discover_event_empty():
    assert gitlab.discover_event(issue_mocks.INVALID_PAYLOAD) is None


def test_discover_event_from_first_discover():
    event = gitlab.discover_event(issue_mocks.ISSUE_OPENED)
    assert isinstance(event, issue.IssueOpened) is True


def test_discover_event_from_last_discover():
    event = gitlab.discover_event(merge_request_mocks.MERGE_REQUEST_OPENED)
    assert isinstance(event, merge_request.MergeRequestOpened) is True
