from gitlab.events import Event


def header(event: Event):
    header = {}

    if event.title is not None:
        header['title'] = event.title

    if event.subtitle is not None:
        header['subtitle'] = event.subtitle

    if event.content is not None:
        header['content'] = event.content

    if event.image is not None:
        header['imageUrl'] = event.image
        if event.image_style is not None:
            header['imageStyle'] = event.image_style

    return header


def footer(event: Event):
    return text_button('View', event.url)


def key_value(top=None, content=None, bottom=None, image=None, click_through=None):
    widget = {}

    if top is not None:
        widget['topLabel'] = top

    if content is not None:
        widget['content'] = content
        widget['contentMultiline'] = "true"

    if bottom is not None:
        widget['bottomLabel'] = bottom

    if image is not None:
        widget['iconUrl'] = image

    if click_through is not None:
        widget['onClick'] = {
            "openLink": {
                "url": click_through
            }
        }

    return {"keyValue": widget}


def text_button(title, url):
    return {
        'buttons': [
            {
                'textButton': {
                    'text': title,
                    'onClick': {
                        'openLink': {
                            'url': url
                        }
                    }
                }
            }
        ]
    }
