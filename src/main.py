from flask import Flask
from json import dumps
from gitlab.blueprint import gitlab_blueprint

flask_app = Flask(__name__)
flask_app.register_blueprint(gitlab_blueprint)


@flask_app.errorhandler(404)
def page_not_found(error):
    return dumps({'message': '404, oops'}), 404
